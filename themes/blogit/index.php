<?php get_header(); ?>

		<div id="content">

			<div id="primary">
			
				 <?php if( have_posts() ) : ?>
	           <!-- Yes, we have content! -->
             
             <?php if(is_archive()) : ?>

	               <h1 class="archive_title"><?php single_month_title(' '); ?></h1>

              <?php endif; ?>
             <?php while ( have_posts() ) : the_post(); ?>

	               <article>
      
      		          <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

		                <?php if(get_post_type() == 'post') : ?>
			                 <?php the_date(); ?>
		                <?php endif; ?>
                     	<p>	
		                	<?php echo get_the_excerpt(); ?>
						</p>
          	     </article>

	         <?php endwhile; ?>


	        <?php else: ?>              
	            <!-- Sorry, no content here! -->
              <h1>No content!!!</h1>

	        <?php endif; ?>

			</div><!-- /primary -->

			<div id="secondary">

				<h3>Menu</h3>

				<?php wp_nav_menu(); ?>

				<h3>Archive</h3>

				<ul class="menu">
					<?php wp_get_archives(); ?>
				</ul>

			</div><!-- /secondary -->

		</div><!-- /content -->
<?php get_footer(); ?>
		