 <?php get_header('page'); ?>
			<div id="primary" class="page">
                
								  <?php if( have_posts() ) : ?>
	           <!-- Yes, we have content! -->
             
             

	               <h1 class="archive_title"><?php printf( __( 'Your search: %s'), get_search_query() ); ?></h1>

             
             <?php while ( have_posts() ) : the_post(); ?>

	               <article>
      
      		          <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

		                <?php if(get_post_type() == 'post') : ?>
			                 <?php the_date(); ?>
		                <?php endif; ?>
                     
		                <?php the_excerpt(); ?>

          	     </article>

	         <?php endwhile; ?>


	        <?php else: ?>              
	            <!-- Sorry, no content here! -->
              <h1>No content!!!</h1>

	        <?php endif; ?>




			</div><!-- /primary -->

			<div id="secondary">

        <?php get_sidebar('page'); ?>

			</div><!-- /secondary -->
  <?php get_footer(); ?>
