 <?php get_header(); ?>
			<div id="primary">
                
			<?php if( have_posts() ) : ?>
	           <!-- Yes, we have content! -->
             
             <?php if(is_archive()) : ?>

	               <h1 class="archive_title"><?php single_month_title(' '); ?></h1>

              <?php endif; ?>
             <?php while ( have_posts() ) : the_post(); ?>

	               <article>
      
      		          <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

		                <?php if(get_post_type() == 'post') : ?>
			                 <?php the_date(); ?>
		                <?php endif; ?>
                     
                        <?php if(has_post_thumbnail()) : ?>
                            <div class="list_featured_img">  
                              <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                            </div>
                        <?php endif; ?>
                     
		                <?php the_excerpt(); ?>

          	     </article>

	         <?php endwhile; ?>


	        <?php else: ?>              
	            <!-- Sorry, no content here! -->
              <h1>No content!!!</h1>

	        <?php endif; ?>




			</div><!-- /primary -->

			<div id="secondary">

        <?php get_sidebar(); ?>

			</div><!-- /secondary -->
  <?php get_footer(); ?>
