<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Blogit!</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	
	<link type="text/css" rel="stylesheet" href="<?=get_template_directory_uri()?>/style.css" />
<?php wp_head(); ?>
</head>
<body>
	<div class="container">

		<header class="main">

			<span class="site_title">Blogit!</span>

		</header>