<?php

/* Westland Functions */

//Add menu functionality
if(function_exists('register_nav_menus')) {
  
  register_nav_menus();
}



function westland_scripts() {
	wp_enqueue_style( 'westland-css', get_stylesheet_uri(), ['bootstrap-css'] );
	
	wp_enqueue_style( 'bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css' );

}
add_action( 'wp_enqueue_scripts', 'westland_scripts' );
